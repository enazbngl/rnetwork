import networkx as nx
from Bio.PDB.PDBParser import PDBParser
from Bio.PDB import Residue
from collections import OrderedDict
import math
import matplotlib.pyplot as plt
import csv
import pandas

def createRNetwork(pdb, cutoff, reTimeFile, outputFileName, talky=False):
	'''creates a residue network having edges within given cutoff range'''

	def get_residues(pdb):

		#gets the residue list from the pdb using PDBParser

		parser = PDBParser()
		structure = parser.get_structure('prot', pdb)
		# residue list in pdb
		residueList = list(structure.get_residues())
		#temp list for erasing the unnecessary atoms (water, hetatom etc.)
		garbageList = []
		for res in residueList:
			if res.get_id()[0] != ' ':
				garbageList.append(res)
		#erase the residues with false id
		for res in garbageList:
			residueList.remove(res)
		return residueList


	def calculate_distance(coords1, coords2):
		#calculates the distance between two atoms
		xDistance = ((coords1[0]) - (coords2[0]))**2
		yDistance = ((coords1[1]) - (coords2[1]))**2
		zDistance = ((coords1[2]) - (coords2[2]))**2
		xyzDistance = math.sqrt((xDistance)+(yDistance)+(zDistance))

		return xyzDistance

	# def withinCutoff(res1, res2, cutoff,talky=False):
	# 	#decides whether the atom is within the cutoff range or not. If it is, returns True.
	# 	for atom1 in res1:
	# 		atom1coords = atom1.get_coord()
	# 		for atom2 in res2:
	# 			atom2coords = atom2.get_coord()
	# 			distance = calculate_distance(atom1coords, atom2coords)
	# 			if (distance <= cutoff):
	# 				return True

	
	print 'Cutoff is %d' %(cutoff)

	if talky == True:
		print 'Initializing...'
	
	#start creating network
	residueList = get_residues(pdb)
	network = nx.DiGraph()

	#create a nodelist for proper naming
	nodeList = []

	for chunk in range(0, len(residueList)):
		node = residueList[chunk]
		nodeNumber = str(chunk +1)
		nodeID = node.get_resname() + nodeNumber
		nodeList.append(nodeID)
	#extract the positions of calpas and add nodes with position and retime attributes.
	openReTimes = open(reTimeFile, 'r')
	reTimeList = list()
	reTimes = openReTimes.readlines()
	for j in range(0, len(reTimes)):
		chunk = reTimes[j]
		reTimeList.append(chunk)

	for res in range(0, len(residueList)):
		node = residueList[res]
		pos = node['CA'].get_coord()
		posx = pos[0]
		posy = pos[1]
		posz = pos[2]
		# posList = list()
		# posList.append(posx)
		# posList.append(posy)
		# posList.append(posz)
		network.add_node(nodeList[res], posx = ('%f' %posx), posy = ('%f' %posy), posz = ('%f' %posz), retime = (reTimeList[res]))
	if talky == True:
		print 'Adding nodes to network...'
	print 'Total # of residue: ', len(residueList)
	print 'Total # of nodes: ', len(nodeList)
	print 'Calculating distances...'
	print 'Assigning directions...'
	#add edges between nodes if they are within cutoff range and decide the direction using retimes of nodes
	# if the retime of node A is less than the node B direction is from A to B. 
	for i in range(0, len(residueList)):
		residue1 = residueList[i]
		coords1 = residue1['CA'].get_coord()
		retime1 = network.node[nodeList[i]]['retime']
		for j in range(i+1,len(residueList)):
			residue2 = residueList[j]
			coords2 = residue2['CA'].get_coord()
			retime2 = network.node[nodeList[j]]['retime']
			distance = calculate_distance(coords1, coords2)
			if distance <= cutoff:
				if retime1 <= retime2:
					network.add_edge(nodeList[i], nodeList[j])
				else:
					network.add_edge(nodeList[j], nodeList[i])
			
	if talky == True:

		print 'Adding edges between nodes...'

	print network.nodes()
	# for i in range(0, len(residueList)):
	# 	#decides cutoff range of Calphas
	# 	residue1 = residueList[i]
	# 	coords1 = residue1['CA'].get_coord()
	# 	for j in range((i+1), len(residueList)):
	# 		residue2 = residueList[j]
	# 		coords2 = residue2['CA'].get_coord()
	# 		distance = calculate_distance(coords1, coords2)
	# 		if distance <= cutoff:
	# 			network.add_edge(residue1, residue2)

	# edgeList = []


	# for a,b in network.edges():
	# 	#prints out the edge list to screen
	# 	resofEdge = a.get_resname() + str(a.get_id()[1]) + ' ' + b.get_resname() + str(b.get_id()[1])
	# 	edgeList.append(resofEdge)

	#print len(network.edges())

	nx.write_gml(network, '%s' %outputFileName)
	if talky == True:
		print 'Network construction is complete.'
	#print network.node
	# position = nx.get_node_attributes(network, 'pos')
	# nx.draw_networkx_nodes(network,position)
	# nx.draw_networkx_edges(network,position)
	# plt.show()

	return network

def pairNetworks(network, cutoff, source, target, pairNetworkName, talky = False):

	def calculate_distance(coords1, coords2):
		#calculates the distance between two atoms
		xDistance = (float(coords1[0]) - float(coords2[0]))**2
		yDistance = (float(coords1[1]) - float(coords2[1]))**2
		zDistance = (float(coords1[2]) - float(coords2[2]))**2
		xyzDistance = math.sqrt((xDistance)+(yDistance)+(zDistance))

		return xyzDistance
	
	sourceRetime = network.node['%s' %(source)]['retime']
	targetRetime = network.node['%s' %(target)]['retime']


	newNetwork = nx.DiGraph()
	print 'Initializing source-target pair network...'

	nodes = network.nodes()
	print 'Checking retime of nodes'

	for x in range(0, len(nodes)):
		nodeRetime = network.node[nodes[x]]['retime']
		if nodeRetime >= targetRetime:
			if nodes[x] == target:
				continue
			else:
				network.remove_node(nodes[x])
				'Removing node %s' %(nodes[x])
		else:
			continue

	for node in network.nodes():
		newNetwork.add_node(node)
	for i in range(0, len(network.nodes())):
		nodes = network.nodes()
		node1 = nodes[i]
		posx = network.node[nodes[i]]['posx']
		posy = network.node[nodes[i]]['posy']
		posz = network.node[nodes[i]]['posz']
		coords1 = [posx, posy, posz]
		retime1 = network.node[nodes[i]]['retime']
		for j in range(i+1,len(network.nodes())):
			node2 = nodes[j]
			posx = network.node[nodes[j]]['posx']
			posy = network.node[nodes[j]]['posy']
			posz = network.node[nodes[j]]['posz']
			coords2 = [posx, posy, posz]
			retime2 = network.node[nodes[j]]['retime']
			distance = calculate_distance(coords1, coords2)
			if distance <= cutoff:
				if retime1 <= retime2:
					newNetwork.add_edge(node1, node2)
				else:
					newNetwork.add_edge(node2, node1)


	while True:

		total_node_number = len(network.nodes())
		nodes = network.nodes()
		junknodelist = list()
		
		for i in range(0, len(network.nodes())):
			chunk = nodes[i]
			inDegree = newNetwork.in_degree('%s' %(chunk))
			print 'Checking in-degree of %s' %(chunk)
			
			if inDegree == 0:
				if chunk == source:
					pass
				else: 
					junknodelist.append(chunk)
					print 'Indegree of %s is %f' %(chunk, inDegree )
					
			else:
				continue

		for junknode in junknodelist:
			network.remove_node(junknode)
			print 'Removing node %s' %(junknode)
			
		newNetwork.clear()

		for node in network.nodes():
			newNetwork.add_node(node)
		for n in range(0, len(network.nodes())):
			nodes = network.nodes()
			node1 = nodes[n]
			posx = network.node[nodes[n]]['posx']
			posy = network.node[nodes[n]]['posy']
			posz = network.node[nodes[n]]['posz']
			coords1 = [posx, posy, posz]
			retime1 = network.node[nodes[n]]['retime']
			for m in range(n+1,len(network.nodes())):
				node2 = nodes[m]
				posx = network.node[nodes[m]]['posx']
				posy = network.node[nodes[m]]['posy']
				posz = network.node[nodes[m]]['posz']
				coords2 = [posx, posy, posz]
				retime2 = network.node[nodes[m]]['retime']
				distance = calculate_distance(coords1, coords2)
				if distance <= cutoff:
					if retime1 <= retime2:
						newNetwork.add_edge(node1, node2)
					else:
						newNetwork.add_edge(node2, node1)

		total_node_number_2 = len(network.nodes())

		if total_node_number == total_node_number_2:
			print 'End of in-degree check'
			break


	while True:
		total_node_number = len(network.nodes())
		junknodelist = list()

		for stuff in network.nodes():
			outDegree = newNetwork.out_degree('%s' %(stuff))
			print 'Checking out-degree of %s' %(stuff)
			
			if outDegree == 0:
				if stuff == target:
					pass 
				else:
					junknodelist.append(stuff)
					print 'Outdegree of %s is %f' %(stuff, outDegree )
			else:
				continue

		for junknode in junknodelist:
			network.remove_node(junknode)
			print 'Removing node %s' %(junknode)

		newNetwork.clear()

		for node in network.nodes():
			newNetwork.add_node(node)
		for a in range(0, len(network.nodes())):
			nodes = network.nodes()
			node1 = nodes[a]
			posx = network.node[nodes[a]]['posx']
			posy = network.node[nodes[a]]['posy']
			posz = network.node[nodes[a]]['posz']
			coords1 = [posx, posy, posz]
			retime1 = network.node[nodes[a]]['retime']
			for b in range(a+1,len(network.nodes())):
				node2 = nodes[b]
				posx = network.node[nodes[b]]['posx']
				posy = network.node[nodes[b]]['posy']
				posz = network.node[nodes[b]]['posz']
				coords2 = [posx, posy, posz]
				retime2 = network.node[nodes[b]]['retime']
				distance = calculate_distance(coords1, coords2)
				if distance <= cutoff:
					if retime1 <= retime2:
						newNetwork.add_edge(node1, node2)
					else:
						newNetwork.add_edge(node2, node1)

		total_node_number_2 = len(network.nodes())

		if total_node_number == total_node_number_2:
			print 'End of out-degree check'
			break

	print 'Total node number of source-target pair network is : ' , len(network.nodes())
	print 'Check node number : ' , len(newNetwork.nodes())
	print 'Done! Go to Cytoscape...'



	nx.write_gml(newNetwork, '%s' %pairNetworkName)


def runTest(pdb, cutoff, reTimeFile, outputFileName, source, target, pairNetworkName):

	network = createRNetwork(pdb, cutoff, reTimeFile, outputFileName, True)
	pairNetworks(network, cutoff, source, target, pairNetworkName)


	
