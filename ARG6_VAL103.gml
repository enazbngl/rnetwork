graph [
  directed 1
  node [
    id 0
    label "VAL103"
    posz "-29.311001"
    retime "2.900000000000000000e+01
"
    posx "15.763000"
    posy "-25.358000"
  ]
  node [
    id 1
    label "PRO290"
    posz "-7.758000"
    retime "2.560000000000000000e+02
"
    posx "30.992001"
    posy "-1.880000"
  ]
  node [
    id 2
    label "ASP238"
    posz "-19.976999"
    retime "1.530000000000000000e+02
"
    posx "26.000000"
    posy "-4.075000"
  ]
  node [
    id 3
    label "VAL248"
    posz "9.300000"
    retime "2.880000000000000000e+02
"
    posx "21.431999"
    posy "-12.971000"
  ]
  node [
    id 4
    label "PHE306"
    posz "-11.854000"
    retime "1.290000000000000000e+02
"
    posx "4.005000"
    posy "-4.609000"
  ]
  node [
    id 5
    label "PRO47"
    posz "-34.543999"
    retime "1.280000000000000000e+02
"
    posx "17.774000"
    posy "-3.487000"
  ]
  node [
    id 6
    label "VAL152"
    posz "-36.707001"
    retime "1.780000000000000000e+02
"
    posx "-4.991000"
    posy "-21.454000"
  ]
  node [
    id 7
    label "PHE384"
    posz "-31.000999"
    retime "1.840000000000000000e+02
"
    posx "-10.004000"
    posy "-12.851000"
  ]
  node [
    id 8
    label "LEU341"
    posz "-16.271999"
    retime "1.360000000000000000e+02
"
    posx "15.331000"
    posy "-0.481000"
  ]
  node [
    id 9
    label "LEU340"
    posz "-16.898001"
    retime "1.280000000000000000e+02
"
    posx "11.790000"
    posy "-1.651000"
  ]
  node [
    id 10
    label "ALA182"
    posz "-21.047001"
    retime "1.390000000000000000e+02
"
    posx "28.163000"
    posy "-17.768999"
  ]
  node [
    id 11
    label "THR240"
    posz "-17.679001"
    retime "1.370000000000000000e+02
"
    posx "24.971001"
    posy "-8.978000"
  ]
  node [
    id 12
    label "TYR7"
    posz "-29.198000"
    retime "0.000000000000000000e+00
"
    posx "8.511000"
    posy "-12.069000"
  ]
  node [
    id 13
    label "GLY305"
    posz "-11.162000"
    retime "1.240000000000000000e+02
"
    posx "6.576000"
    posy "-7.335000"
  ]
  node [
    id 14
    label "THR41"
    posz "-34.737000"
    retime "2.410000000000000000e+02
"
    posx "8.072000"
    posy "9.104000"
  ]
  node [
    id 15
    label "GLY237"
    posz "-18.282000"
    retime "1.760000000000000000e+02
"
    posx "22.886999"
    posy "-2.596000"
  ]
  node [
    id 16
    label "LYS146"
    posz "-34.074001"
    retime "2.210000000000000000e+02
"
    posx "-13.001000"
    posy "-19.208000"
  ]
  node [
    id 17
    label "THR143"
    posz "-29.599001"
    retime "1.720000000000000000e+02
"
    posx "-11.825000"
    posy "-17.187000"
  ]
  node [
    id 18
    label "GLU312"
    posz "-13.369000"
    retime "2.240000000000000000e+02
"
    posx "6.614000"
    posy "6.905000"
  ]
  node [
    id 19
    label "GLN224"
    posz "4.750000"
    retime "1.660000000000000000e+02
"
    posx "18.240000"
    posy "-21.910000"
  ]
  node [
    id 20
    label "TRP217"
    posz "1.749000"
    retime "1.430000000000000000e+02
"
    posx "22.802000"
    posy "-20.381001"
  ]
  node [
    id 21
    label "GLN226"
    posz "5.038000"
    retime "2.400000000000000000e+02
"
    posx "13.062000"
    posy "-19.313999"
  ]
  node [
    id 22
    label "ASN174"
    posz "-36.632000"
    retime "1.450000000000000000e+02
"
    posx "25.604000"
    posy "-14.701000"
  ]
  node [
    id 23
    label "ALA245"
    posz "-1.205000"
    retime "2.650000000000000000e+02
"
    posx "21.323000"
    posy "-13.725000"
  ]
  node [
    id 24
    label "ALA246"
    posz "2.434000"
    retime "2.490000000000000000e+02
"
    posx "21.639999"
    posy "-12.660000"
  ]
  node [
    id 25
    label "PRO267"
    posz "-15.015000"
    retime "1.880000000000000000e+02
"
    posx "31.716000"
    posy "-26.136000"
  ]
  node [
    id 26
    label "PRO269"
    posz "-8.700000"
    retime "1.900000000000000000e+02
"
    posx "28.747999"
    posy "-25.485001"
  ]
  node [
    id 27
    label "ARG219"
    posz "7.619000"
    retime "1.870000000000000000e+02
"
    posx "23.934999"
    posy "-23.747000"
  ]
  node [
    id 28
    label "VAL247"
    posz "5.776000"
    retime "2.540000000000000000e+02
"
    posx "21.316999"
    posy "-14.399000"
  ]
  node [
    id 29
    label "PRO43"
    posz "-36.474998"
    retime "1.710000000000000000e+02
"
    posx "7.415000"
    posy "3.389000"
  ]
  node [
    id 30
    label "TYR257"
    posz "6.658000"
    retime "2.630000000000000000e+02
"
    posx "27.400999"
    posy "-20.926001"
  ]
  node [
    id 31
    label "GLY56"
    posz "-45.438000"
    retime "2.380000000000000000e+02
"
    posx "19.205000"
    posy "-6.903000"
  ]
  node [
    id 32
    label "THR362"
    posz "-6.845000"
    retime "2.790000000000000000e+02
"
    posx "-1.164000"
    posy "0.679000"
  ]
  node [
    id 33
    label "GLN255"
    posz "9.360000"
    retime "1.850000000000000000e+02
"
    posx "31.974001"
    posy "-20.416000"
  ]
  node [
    id 34
    label "LEU272"
    posz "-0.914000"
    retime "2.710000000000000000e+02
"
    posx "31.813000"
    posy "-19.840000"
  ]
  node [
    id 35
    label "LEU270"
    posz "-6.401000"
    retime "2.280000000000000000e+02
"
    posx "30.752001"
    posy "-23.184999"
  ]
  node [
    id 36
    label "ARG181"
    posz "-24.666000"
    retime "1.000000000000000000e+02
"
    posx "29.066999"
    posy "-17.223000"
  ]
  node [
    id 37
    label "CYS301"
    posz "-10.944000"
    retime "2.100000000000000000e+02
"
    posx "16.615000"
    posy "1.648000"
  ]
  node [
    id 38
    label "THR216"
    posz "-1.802000"
    retime "1.230000000000000000e+02
"
    posx "21.461000"
    posy "-20.476000"
  ]
  node [
    id 39
    label "SER42"
    posz "-37.827000"
    retime "2.000000000000000000e+02
"
    posx "8.183000"
    posy "6.870000"
  ]
  node [
    id 40
    label "LEU206"
    posz "-10.577000"
    retime "1.520000000000000000e+02
"
    posx "27.885000"
    posy "-10.140000"
  ]
  node [
    id 41
    label "ALA149"
    posz "-37.339001"
    retime "1.990000000000000000e+02
"
    posx "-12.714000"
    posy "-23.247000"
  ]
  node [
    id 42
    label "GLY207"
    posz "-14.286000"
    retime "1.430000000000000000e+02
"
    posx "28.576000"
    posy "-9.939000"
  ]
  node [
    id 43
    label "LYS176"
    posz "-31.868000"
    retime "2.210000000000000000e+02
"
    posx "27.886000"
    posy "-16.735001"
  ]
  node [
    id 44
    label "THR138"
    posz "-21.562000"
    retime "2.130000000000000000e+02
"
    posx "-16.009001"
    posy "-18.577999"
  ]
  node [
    id 45
    label "GLU229"
    posz "1.653000"
    retime "1.430000000000000000e+02
"
    posx "16.604000"
    posy "-12.864000"
  ]
  node [
    id 46
    label "GLU222"
    posz "5.488000"
    retime "2.600000000000000000e+02
"
    posx "21.136999"
    posy "-27.547001"
  ]
  node [
    id 47
    label "GLN72"
    posz "-36.185001"
    retime "1.910000000000000000e+02
"
    posx "-4.298000"
    posy "-3.183000"
  ]
  node [
    id 48
    label "GLU76"
    posz "-32.917000"
    retime "1.890000000000000000e+02
"
    posx "-9.261000"
    posy "-5.146000"
  ]
  node [
    id 49
    label "ARG17"
    posz "-25.530001"
    retime "2.230000000000000000e+02
"
    posx "-5.398000"
    posy "9.655000"
  ]
  node [
    id 50
    label "ARG14"
    posz "-23.422001"
    retime "1.530000000000000000e+02
"
    posx "-5.920000"
    posy "4.628000"
  ]
  node [
    id 51
    label "ILE283"
    posz "-6.820000"
    retime "1.890000000000000000e+02
"
    posx "12.074000"
    posy "-1.684000"
  ]
  node [
    id 52
    label "THR383"
    posz "-34.271000"
    retime "1.150000000000000000e+02
"
    posx "-8.267000"
    posy "-11.866000"
  ]
  node [
    id 53
    label "ALA40"
    posz "-32.452000"
    retime "2.460000000000000000e+02
"
    posx "7.646000"
    posy "6.094000"
  ]
  node [
    id 54
    label "ARG79"
    posz "-28.961000"
    retime "2.130000000000000000e+02
"
    posx "-12.266000"
    posy "-3.981000"
  ]
  node [
    id 55
    label "VAL231"
    posz "-4.448000"
    retime "1.350000000000000000e+02
"
    posx "16.611000"
    posy "-10.400000"
  ]
  node [
    id 56
    label "CYS203"
    posz "-1.153000"
    retime "2.650000000000000000e+02
"
    posx "26.587000"
    posy "-13.627000"
  ]
  node [
    id 57
    label "TYR286"
    posz "-7.600000"
    retime "1.760000000000000000e+02
"
    posx "22.025000"
    posy "-0.136000"
  ]
  node [
    id 58
    label "ASP372"
    posz "-0.752000"
    retime "2.530000000000000000e+02
"
    posx "22.913000"
    posy "1.779000"
  ]
  node [
    id 59
    label "ASP119"
    posz "-18.127001"
    retime "1.310000000000000000e+02
"
    posx "-5.376000"
    posy "-7.447000"
  ]
  node [
    id 60
    label "THR187"
    posz "-6.587000"
    retime "2.030000000000000000e+02
"
    posx "31.267000"
    posy "-14.018000"
  ]
  node [
    id 61
    label "THR258"
    posz "3.012000"
    retime "2.890000000000000000e+02
"
    posx "28.191999"
    posy "-21.646999"
  ]
  node [
    id 62
    label "SER88"
    posz "-20.783001"
    retime "2.840000000000000000e+02
"
    posx "-17.158001"
    posy "-0.065000"
  ]
  node [
    id 63
    label "ARG256"
    posz "9.681000"
    retime "1.850000000000000000e+02
"
    posx "28.900000"
    posy "-22.657000"
  ]
  node [
    id 64
    label "ILE311"
    posz "-14.299000"
    retime "2.380000000000000000e+02
"
    posx "4.716000"
    posy "3.742000"
  ]
  node [
    id 65
    label "GLN218"
    posz "3.881000"
    retime "2.140000000000000000e+02
"
    posx "23.323999"
    posy "-23.464001"
  ]
  node [
    id 66
    label "ASN300"
    posz "-12.098000"
    retime "1.990000000000000000e+02
"
    posx "20.195000"
    posy "1.832000"
  ]
  node [
    id 67
    label "THR24"
    posz "-29.702000"
    retime "1.400000000000000000e+01
"
    posx "6.909000"
    posy "-5.520000"
  ]
  node [
    id 68
    label "SER13"
    posz "-24.709999"
    retime "1.340000000000000000e+02
"
    posx "-5.794000"
    posy "1.070000"
  ]
  node [
    id 69
    label "ILE142"
    posz "-28.239000"
    retime "2.570000000000000000e+02
"
    posx "-15.291000"
    posy "-17.930000"
  ]
  node [
    id 70
    label "SER287"
    posz "-9.698000"
    retime "2.580000000000000000e+02
"
    posx "25.121000"
    posy "0.524000"
  ]
  node [
    id 71
    label "GLU58"
    posz "-47.250999"
    retime "2.210000000000000000e+02
"
    posx "14.078000"
    posy "-6.782000"
  ]
  node [
    id 72
    label "TYR84"
    posz "-24.570000"
    retime "2.690000000000000000e+02
"
    posx "-16.614000"
    posy "-10.477000"
  ]
  node [
    id 73
    label "ASP220"
    posz "8.650000"
    retime "2.210000000000000000e+02
"
    posx "25.743999"
    posy "-26.969000"
  ]
  node [
    id 74
    label "PHE298"
    posz "-13.956000"
    retime "2.620000000000000000e+02
"
    posx "25.892000"
    posy "3.863000"
  ]
  node [
    id 75
    label "GLY175"
    posz "-32.952999"
    retime "1.820000000000000000e+02
"
    posx "25.712999"
    posy "-13.781000"
  ]
  node [
    id 76
    label "GLN65"
    posz "-42.169998"
    retime "1.630000000000000000e+02
"
    posx "4.930000"
    posy "-4.654000"
  ]
  node [
    id 77
    label "LEU230"
    posz "-2.065000"
    retime "1.320000000000000000e+02
"
    posx "17.025000"
    posy "-13.354000"
  ]
  node [
    id 78
    label "VAL261"
    posz "-6.734000"
    retime "2.110000000000000000e+02
"
    posx "26.264000"
    posy "-20.875000"
  ]
  node [
    id 79
    label "VAL303"
    posz "-11.509000"
    retime "1.820000000000000000e+02
"
    posx "11.211000"
    posy "-1.810000"
  ]
  node [
    id 80
    label "ALA140"
    posz "-24.555000"
    retime "2.080000000000000000e+02
"
    posx "-11.609000"
    posy "-17.094000"
  ]
  node [
    id 81
    label "HIS327"
    posz "-19.924999"
    retime "1.760000000000000000e+02
"
    posx "15.037000"
    posy "3.526000"
  ]
  node [
    id 82
    label "GLN141"
    posz "-25.566000"
    retime "2.290000000000000000e+02
"
    posx "-13.688000"
    posy "-20.136999"
  ]
  node [
    id 83
    label "GLU128"
    posz "-26.257000"
    retime "2.800000000000000000e+01
"
    posx "2.495000"
    posy "-27.983999"
  ]
  node [
    id 84
    label "PRO20"
    posz "-28.266001"
    retime "2.010000000000000000e+02
"
    posx "-3.996000"
    posy "1.788000"
  ]
  node [
    id 85
    label "ILE213"
    posz "-11.690000"
    retime "1.210000000000000000e+02
"
    posx "21.302999"
    posy "-18.629999"
  ]
  node [
    id 86
    label "THR228"
    posz "3.248000"
    retime "1.820000000000000000e+02
"
    posx "17.443001"
    posy "-16.194000"
  ]
  node [
    id 87
    label "TYR118"
    posz "-20.971001"
    retime "1.290000000000000000e+02
"
    posx "-5.919000"
    posy "-9.899000"
  ]
  node [
    id 88
    label "THR64"
    posz "-40.127998"
    retime "1.650000000000000000e+02
"
    posx "7.987000"
    posy "-3.668000"
  ]
  node [
    id 89
    label "THR69"
    posz "-39.382000"
    retime "1.410000000000000000e+02
"
    posx "-0.487000"
    posy "-5.213000"
  ]
  node [
    id 90
    label "HIS191"
    posz "6.184000"
    retime "2.530000000000000000e+02
"
    posx "30.087999"
    posy "-9.303000"
  ]
  node [
    id 91
    label "LYS268"
    posz "-11.425000"
    retime "1.860000000000000000e+02
"
    posx "30.881001"
    posy "-27.103001"
  ]
  node [
    id 92
    label "MET12"
    posz "-24.190001"
    retime "1.140000000000000000e+02
"
    posx "-2.597000"
    posy "-0.950000"
  ]
  node [
    id 93
    label "ASN77"
    posz "-30.195999"
    retime "1.140000000000000000e+02
"
    posx "-8.489000"
    posy "-7.699000"
  ]
  node [
    id 94
    label "GLY91"
    posz "-19.143000"
    retime "2.890000000000000000e+02
"
    posx "-11.330000"
    posy "2.030000"
  ]
  node [
    id 95
    label "HIS260"
    posz "-3.168000"
    retime "1.820000000000000000e+02
"
    posx "26.236000"
    posy "-22.173000"
  ]
  node [
    id 96
    label "TRP371"
    posz "-3.098000"
    retime "2.300000000000000000e+02
"
    posx "20.809999"
    posy "3.911000"
  ]
  node [
    id 97
    label "GLU46"
    posz "-35.250000"
    retime "1.530000000000000000e+02
"
    posx "14.613000"
    posy "-1.450000"
  ]
  node [
    id 98
    label "TRP274"
    posz "4.424000"
    retime "2.230000000000000000e+02
"
    posx "32.990002"
    posy "-15.405000"
  ]
  node [
    id 99
    label "GLN242"
    posz "-10.805000"
    retime "1.550000000000000000e+02
"
    posx "23.344999"
    posy "-9.388000"
  ]
  node [
    id 100
    label "ILE277"
    posz "-11.226000"
    retime "1.930000000000000000e+02
"
    posx "-5.949000"
    posy "-7.383000"
  ]
  node [
    id 101
    label "LEU130"
    posz "-31.194000"
    retime "2.800000000000000000e+01
"
    posx "0.860000"
    posy "-27.275999"
  ]
  node [
    id 102
    label "LYS68"
    posz "-38.596001"
    retime "1.590000000000000000e+02
"
    posx "2.083000"
    posy "-2.510000"
  ]
  node [
    id 103
    label "GLN96"
    posz "-24.048000"
    retime "2.800000000000000000e+01
"
    posx "-0.128000"
    posy "-9.235000"
  ]
  node [
    id 104
    label "HIS360"
    posz "-9.992000"
    retime "1.860000000000000000e+02
"
    posx "2.127000"
    posy "3.514000"
  ]
  node [
    id 105
    label "SER92"
    posz "-20.063000"
    retime "1.960000000000000000e+02
"
    posx "-7.733000"
    posy "1.246000"
  ]
  node [
    id 106
    label "PHE33"
    posz "-30.561001"
    retime "1.700000000000000000e+01
"
    posx "17.509001"
    posy "-7.872000"
  ]
  node [
    id 107
    label "LYS282"
    posz "-6.849000"
    retime "1.230000000000000000e+02
"
    posx "9.286000"
    posy "-4.260000"
  ]
  node [
    id 108
    label "GLU275"
    posz "4.097000"
    retime "1.620000000000000000e+02
"
    posx "36.750999"
    posy "-14.838000"
  ]
  node [
    id 109
    label "TRP336"
    posz "-17.514000"
    retime "2.700000000000000000e+01
"
    posx "2.876000"
    posy "-11.302000"
  ]
  node [
    id 110
    label "ALA236"
    posz "-14.958000"
    retime "1.370000000000000000e+02
"
    posx "23.419001"
    posy "-4.399000"
  ]
  node [
    id 111
    label "PRO235"
    posz "-15.245000"
    retime "1.240000000000000000e+02
"
    posx "20.115000"
    posy "-6.286000"
  ]
  node [
    id 112
    label "ARG234"
    posz "-12.064000"
    retime "1.220000000000000000e+02
"
    posx "18.513000"
    posy "-7.665000"
  ]
  node [
    id 113
    label "ARG83"
    posz "-25.825001"
    retime "2.430000000000000000e+02
"
    posx "-17.351000"
    posy "-6.934000"
  ]
  node [
    id 114
    label "GLY265"
    posz "-17.115999"
    retime "1.480000000000000000e+02
"
    posx "29.035000"
    posy "-20.606001"
  ]
  node [
    id 115
    label "THR280"
    posz "-7.269000"
    retime "1.580000000000000000e+02
"
    posx "2.625000"
    posy "-3.263000"
  ]
  node [
    id 116
    label "ASP310"
    posz "-16.723000"
    retime "2.370000000000000000e+02
"
    posx "2.002000"
    posy "2.736000"
  ]
  node [
    id 117
    label "ASP61"
    posz "-44.542000"
    retime "1.630000000000000000e+02
"
    posx "10.587000"
    posy "-3.985000"
  ]
  node [
    id 118
    label "MET375"
    posz "-1.802000"
    retime "2.460000000000000000e+02
"
    posx "22.503000"
    posy "-3.655000"
  ]
  node [
    id 119
    label "GLU19"
    posz "-30.339001"
    retime "2.110000000000000000e+02
"
    posx "-5.473000"
    posy "4.613000"
  ]
  node [
    id 120
    label "GLN278"
    posz "-10.012000"
    retime "1.900000000000000000e+02
"
    posx "-3.270000"
    posy "-4.925000"
  ]
  node [
    id 121
    label "VAL25"
    posz "-28.348000"
    retime "1.000000000000000000e+01
"
    posx "10.270000"
    posy "-6.619000"
  ]
  node [
    id 122
    label "VAL28"
    posz "-28.134001"
    retime "1.000000000000000000e+01
"
    posx "17.132999"
    posy "-14.502000"
  ]
  node [
    id 123
    label "ILE95"
    posz "-24.121000"
    retime "1.190000000000000000e+02
"
    posx "-3.476000"
    posy "-7.453000"
  ]
  node [
    id 124
    label "GLN54"
    posz "-41.241001"
    retime "1.820000000000000000e+02
"
    posx "23.407000"
    posy "-7.730000"
  ]
  node [
    id 125
    label "ASP102"
    posz "-27.650999"
    retime "2.000000000000000000e+00
"
    posx "13.041000"
    posy "-23.275999"
  ]
  node [
    id 126
    label "GLU292"
    posz "-6.033000"
    retime "2.760000000000000000e+02
"
    posx "34.194000"
    posy "3.618000"
  ]
  node [
    id 127
    label "ASP223"
    posz "3.127000"
    retime "1.720000000000000000e+02
"
    posx "19.211000"
    posy "-25.225000"
  ]
  node [
    id 128
    label "VAL313"
    posz "-12.384000"
    retime "2.610000000000000000e+02
"
    posx "10.257000"
    posy "6.527000"
  ]
  node [
    id 129
    label "ASP227"
    posz "6.548000"
    retime "2.270000000000000000e+02
"
    posx "15.803000"
    posy "-17.142000"
  ]
  node [
    id 130
    label "LYS370"
    posz "-1.440000"
    retime "2.780000000000000000e+02
"
    posx "17.625000"
    posy "5.127000"
  ]
  node [
    id 131
    label "TRP382"
    posz "-36.544998"
    retime "1.070000000000000000e+02
"
    posx "-6.151000"
    posy "-14.003000"
  ]
  node [
    id 132
    label "TYR342"
    posz "-15.758000"
    retime "1.900000000000000000e+02
"
    posx "15.982000"
    posy "3.255000"
  ]
  node [
    id 133
    label "TYR343"
    posz "-15.915000"
    retime "1.950000000000000000e+02
"
    posx "19.615999"
    posy "4.335000"
  ]
  node [
    id 134
    label "PRO15"
    posz "-23.955000"
    retime "2.430000000000000000e+02
"
    posx "-9.359000"
    posy "6.205000"
  ]
  node [
    id 135
    label "ASP137"
    posz "-19.548000"
    retime "2.480000000000000000e+02
"
    posx "-12.770000"
    posy "-18.733000"
  ]
  node [
    id 136
    label "PRO281"
    posz "-8.473000"
    retime "1.660000000000000000e+02
"
    posx "6.230000"
    posy "-2.744000"
  ]
  node [
    id 137
    label "HIS188"
    posz "-3.117000"
    retime "2.590000000000000000e+02
"
    posx "32.097000"
    posy "-12.748000"
  ]
  node [
    id 138
    label "THR73"
    posz "-35.195000"
    retime "1.230000000000000000e+02
"
    posx "-4.920000"
    posy "-6.814000"
  ]
  node [
    id 139
    label "THR71"
    posz "-34.424000"
    retime "1.370000000000000000e+02
"
    posx "-0.917000"
    posy "-2.997000"
  ]
  node [
    id 140
    label "LEU78"
    posz "-27.540001"
    retime "1.820000000000000000e+02
"
    posx "-8.831000"
    posy "-4.960000"
  ]
  node [
    id 141
    label "ASP39"
    posz "-30.757999"
    retime "1.300000000000000000e+02
"
    posx "4.239000"
    posy "5.675000"
  ]
  node [
    id 142
    label "ASP37"
    posz "-30.959999"
    retime "1.580000000000000000e+02
"
    posx "6.723000"
    posy "0.922000"
  ]
  node [
    id 143
    label "ASP30"
    posz "-22.451000"
    retime "1.000000000000000000e+01
"
    posx "18.214001"
    posy "-12.796000"
  ]
  node [
    id 144
    label "GLY221"
    posz "5.156000"
    retime "2.420000000000000000e+02
"
    posx "24.879000"
    posy "-28.292999"
  ]
  node [
    id 145
    label "ARG373"
    posz "-2.291000"
    retime "2.250000000000000000e+02
"
    posx "26.169001"
    posy "0.513000"
  ]
  node [
    id 146
    label "GLU326"
    posz "-20.207001"
    retime "1.960000000000000000e+02
"
    posx "17.106001"
    posy "6.669000"
  ]
  node [
    id 147
    label "GLU53"
    posz "-38.372002"
    retime "1.330000000000000000e+02
"
    posx "22.162001"
    posy "-5.519000"
  ]
  node [
    id 148
    label "GLU55"
    posz "-42.136002"
    retime "1.580000000000000000e+02
"
    posx "19.861000"
    posy "-8.685000"
  ]
  node [
    id 149
    label "GLY120"
    posz "-16.781000"
    retime "2.190000000000000000e+02
"
    posx "-2.579000"
    posy "-9.660000"
  ]
  node [
    id 150
    label "SER2"
    posz "-25.337000"
    retime "2.600000000000000000e+01
"
    posx "19.885000"
    posy "-24.700001"
  ]
  node [
    id 151
    label "PRO308"
    posz "-15.452000"
    retime "2.560000000000000000e+02
"
    posx "-0.653000"
    posy "-2.458000"
  ]
  node [
    id 152
    label "ASP183"
    posz "-18.658001"
    retime "1.440000000000000000e+02
"
    posx "29.757000"
    posy "-15.275000"
  ]
  node [
    id 153
    label "SER309"
    posz "-17.421000"
    retime "1.590000000000000000e+02
"
    posx "2.268000"
    posy "-0.992000"
  ]
  node [
    id 154
    label "SER304"
    posz "-11.976000"
    retime "1.100000000000000000e+02
"
    posx "9.589000"
    posy "-5.195000"
  ]
  node [
    id 155
    label "ARG6"
    posz "-28.028000"
    retime "0.000000000000000000e+00
"
    posx "10.581000"
    posy "-14.977000"
  ]
  node [
    id 156
    label "PRO276"
    posz "6.960000"
    retime "2.250000000000000000e+02
"
    posx "38.199001"
    posy "-12.733000"
  ]
  node [
    id 157
    label "HIS93"
    posz "-20.823999"
    retime "2.150000000000000000e+02
"
    posx "-7.011000"
    posy "-2.418000"
  ]
  node [
    id 158
    label "TYR302"
    posz "-11.852000"
    retime "1.000000000000000000e+02
"
    posx "14.986000"
    posy "-1.648000"
  ]
  node [
    id 159
    label "ARG273"
    posz "2.676000"
    retime "1.310000000000000000e+02
"
    posx "32.325001"
    posy "-18.700001"
  ]
  node [
    id 160
    label "GLN87"
    posz "-20.299000"
    retime "1.680000000000000000e+02
"
    posx "-16.773001"
    posy "-3.810000"
  ]
  node [
    id 161
    label "VAL361"
    posz "-9.765000"
    retime "2.820000000000000000e+02
"
    posx "-1.668000"
    posy "3.135000"
  ]
  node [
    id 162
    label "ALA150"
    posz "-39.080002"
    retime "2.130000000000000000e+02
"
    posx "-9.893000"
    posy "-21.347000"
  ]
  node [
    id 163
    label "THR271"
    posz "-2.613000"
    retime "1.460000000000000000e+02
"
    posx "30.879000"
    posy "-23.145000"
  ]
  node [
    id 164
    label "ALA153"
    posz "-34.716999"
    retime "1.730000000000000000e+02
"
    posx "-3.043000"
    posy "-24.086000"
  ]
  node [
    id 165
    label "GLN284"
    posz "-6.734000"
    retime "1.090000000000000000e+02
"
    posx "15.859000"
    posy "-2.061000"
  ]
  node [
    id 166
    label "LEU266"
    posz "-14.199000"
    retime "1.470000000000000000e+02
"
    posx "30.474001"
    posy "-22.615000"
  ]
  node [
    id 167
    label "PHE22"
    posz "-29.535000"
    retime "1.170000000000000000e+02
"
    posx "1.420000"
    posy "-1.371000"
  ]
  node [
    id 168
    label "HIS307"
    posz "-13.720000"
    retime "1.510000000000000000e+02
"
    posx "0.698000"
    posy "-4.517000"
  ]
  node [
    id 169
    label "THR190"
    posz "2.427000"
    retime "2.660000000000000000e+02
"
    posx "30.393999"
    posy "-9.547000"
  ]
  node [
    id 170
    label "THR200"
    posz "7.715000"
    retime "2.570000000000000000e+02
"
    posx "24.556999"
    posy "-10.093000"
  ]
  node [
    id 171
    label "ALA381"
    posz "-36.966000"
    retime "1.060000000000000000e+02
"
    posx "-2.485000"
    posy "-13.228000"
  ]
  node [
    id 172
    label "PRO184"
    posz "-15.213000"
    retime "1.520000000000000000e+02
"
    posx "30.111000"
    posy "-16.839001"
  ]
  node [
    id 173
    label "PRO185"
    posz "-12.038000"
    retime "2.190000000000000000e+02
"
    posx "29.283001"
    posy "-14.932000"
  ]
  node [
    id 174
    label "ALA205"
    posz "-8.011000"
    retime "1.940000000000000000e+02
"
    posx "26.893000"
    posy "-12.765000"
  ]
  node [
    id 175
    label "ARG202"
    posz "1.734000"
    retime "1.960000000000000000e+02
"
    posx "25.559000"
    posy "-11.384000"
  ]
  node [
    id 176
    label "LEU160"
    posz "-34.936001"
    retime "2.100000000000000000e+01
"
    posx "7.171000"
    posy "-22.847000"
  ]
  node [
    id 177
    label "ARG97"
    posz "-26.277000"
    retime "2.200000000000000000e+01
"
    posx "1.183000"
    posy "-11.983000"
  ]
  node [
    id 178
    label "GLU377"
    posz "-36.813999"
    retime "1.300000000000000000e+01
"
    posx "8.144000"
    posy "-13.099000"
  ]
  edge [
    source 1
    target 70
  ]
  edge [
    source 1
    target 126
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 4
    target 151
  ]
  edge [
    source 4
    target 115
  ]
  edge [
    source 4
    target 168
  ]
  edge [
    source 4
    target 153
  ]
  edge [
    source 4
    target 120
  ]
  edge [
    source 4
    target 79
  ]
  edge [
    source 4
    target 136
  ]
  edge [
    source 5
    target 147
  ]
  edge [
    source 5
    target 106
  ]
  edge [
    source 5
    target 97
  ]
  edge [
    source 6
    target 162
  ]
  edge [
    source 6
    target 41
  ]
  edge [
    source 7
    target 69
  ]
  edge [
    source 7
    target 48
  ]
  edge [
    source 7
    target 80
  ]
  edge [
    source 7
    target 16
  ]
  edge [
    source 8
    target 37
  ]
  edge [
    source 8
    target 81
  ]
  edge [
    source 8
    target 66
  ]
  edge [
    source 8
    target 79
  ]
  edge [
    source 8
    target 132
  ]
  edge [
    source 8
    target 133
  ]
  edge [
    source 9
    target 8
  ]
  edge [
    source 9
    target 81
  ]
  edge [
    source 9
    target 79
  ]
  edge [
    source 9
    target 132
  ]
  edge [
    source 10
    target 172
  ]
  edge [
    source 10
    target 152
  ]
  edge [
    source 10
    target 114
  ]
  edge [
    source 11
    target 42
  ]
  edge [
    source 11
    target 15
  ]
  edge [
    source 11
    target 40
  ]
  edge [
    source 11
    target 2
  ]
  edge [
    source 11
    target 152
  ]
  edge [
    source 11
    target 99
  ]
  edge [
    source 12
    target 121
  ]
  edge [
    source 12
    target 177
  ]
  edge [
    source 12
    target 178
  ]
  edge [
    source 12
    target 67
  ]
  edge [
    source 13
    target 115
  ]
  edge [
    source 13
    target 168
  ]
  edge [
    source 13
    target 4
  ]
  edge [
    source 13
    target 79
  ]
  edge [
    source 13
    target 136
  ]
  edge [
    source 14
    target 53
  ]
  edge [
    source 16
    target 69
  ]
  edge [
    source 17
    target 69
  ]
  edge [
    source 17
    target 80
  ]
  edge [
    source 17
    target 16
  ]
  edge [
    source 17
    target 82
  ]
  edge [
    source 17
    target 7
  ]
  edge [
    source 18
    target 116
  ]
  edge [
    source 18
    target 64
  ]
  edge [
    source 18
    target 128
  ]
  edge [
    source 19
    target 46
  ]
  edge [
    source 19
    target 65
  ]
  edge [
    source 19
    target 27
  ]
  edge [
    source 19
    target 129
  ]
  edge [
    source 19
    target 86
  ]
  edge [
    source 19
    target 127
  ]
  edge [
    source 19
    target 21
  ]
  edge [
    source 20
    target 61
  ]
  edge [
    source 20
    target 65
  ]
  edge [
    source 20
    target 27
  ]
  edge [
    source 20
    target 86
  ]
  edge [
    source 20
    target 23
  ]
  edge [
    source 20
    target 24
  ]
  edge [
    source 20
    target 95
  ]
  edge [
    source 20
    target 28
  ]
  edge [
    source 20
    target 127
  ]
  edge [
    source 20
    target 19
  ]
  edge [
    source 20
    target 30
  ]
  edge [
    source 22
    target 43
  ]
  edge [
    source 22
    target 75
  ]
  edge [
    source 24
    target 23
  ]
  edge [
    source 24
    target 56
  ]
  edge [
    source 24
    target 3
  ]
  edge [
    source 24
    target 28
  ]
  edge [
    source 24
    target 170
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 26
    target 35
  ]
  edge [
    source 26
    target 78
  ]
  edge [
    source 27
    target 61
  ]
  edge [
    source 27
    target 46
  ]
  edge [
    source 27
    target 65
  ]
  edge [
    source 27
    target 144
  ]
  edge [
    source 27
    target 73
  ]
  edge [
    source 27
    target 30
  ]
  edge [
    source 28
    target 23
  ]
  edge [
    source 28
    target 3
  ]
  edge [
    source 28
    target 170
  ]
  edge [
    source 29
    target 39
  ]
  edge [
    source 29
    target 53
  ]
  edge [
    source 29
    target 14
  ]
  edge [
    source 30
    target 61
  ]
  edge [
    source 32
    target 161
  ]
  edge [
    source 33
    target 98
  ]
  edge [
    source 33
    target 61
  ]
  edge [
    source 33
    target 63
  ]
  edge [
    source 33
    target 30
  ]
  edge [
    source 34
    target 61
  ]
  edge [
    source 35
    target 34
  ]
  edge [
    source 36
    target 10
  ]
  edge [
    source 36
    target 43
  ]
  edge [
    source 36
    target 152
  ]
  edge [
    source 38
    target 65
  ]
  edge [
    source 38
    target 19
  ]
  edge [
    source 38
    target 86
  ]
  edge [
    source 38
    target 23
  ]
  edge [
    source 38
    target 78
  ]
  edge [
    source 38
    target 95
  ]
  edge [
    source 38
    target 127
  ]
  edge [
    source 38
    target 20
  ]
  edge [
    source 39
    target 53
  ]
  edge [
    source 39
    target 14
  ]
  edge [
    source 40
    target 173
  ]
  edge [
    source 40
    target 174
  ]
  edge [
    source 40
    target 60
  ]
  edge [
    source 40
    target 99
  ]
  edge [
    source 41
    target 162
  ]
  edge [
    source 41
    target 16
  ]
  edge [
    source 42
    target 172
  ]
  edge [
    source 42
    target 173
  ]
  edge [
    source 42
    target 174
  ]
  edge [
    source 42
    target 40
  ]
  edge [
    source 42
    target 152
  ]
  edge [
    source 42
    target 99
  ]
  edge [
    source 44
    target 135
  ]
  edge [
    source 44
    target 82
  ]
  edge [
    source 44
    target 69
  ]
  edge [
    source 45
    target 86
  ]
  edge [
    source 45
    target 23
  ]
  edge [
    source 45
    target 24
  ]
  edge [
    source 45
    target 129
  ]
  edge [
    source 45
    target 28
  ]
  edge [
    source 48
    target 54
  ]
  edge [
    source 48
    target 47
  ]
  edge [
    source 49
    target 134
  ]
  edge [
    source 50
    target 84
  ]
  edge [
    source 50
    target 157
  ]
  edge [
    source 50
    target 134
  ]
  edge [
    source 50
    target 105
  ]
  edge [
    source 50
    target 94
  ]
  edge [
    source 50
    target 119
  ]
  edge [
    source 50
    target 49
  ]
  edge [
    source 51
    target 37
  ]
  edge [
    source 52
    target 138
  ]
  edge [
    source 52
    target 17
  ]
  edge [
    source 52
    target 48
  ]
  edge [
    source 52
    target 7
  ]
  edge [
    source 54
    target 113
  ]
  edge [
    source 55
    target 23
  ]
  edge [
    source 55
    target 45
  ]
  edge [
    source 56
    target 23
  ]
  edge [
    source 56
    target 169
  ]
  edge [
    source 57
    target 118
  ]
  edge [
    source 57
    target 66
  ]
  edge [
    source 57
    target 37
  ]
  edge [
    source 57
    target 58
  ]
  edge [
    source 57
    target 96
  ]
  edge [
    source 57
    target 70
  ]
  edge [
    source 57
    target 145
  ]
  edge [
    source 58
    target 130
  ]
  edge [
    source 59
    target 149
  ]
  edge [
    source 59
    target 151
  ]
  edge [
    source 59
    target 100
  ]
  edge [
    source 59
    target 157
  ]
  edge [
    source 60
    target 173
  ]
  edge [
    source 60
    target 56
  ]
  edge [
    source 60
    target 137
  ]
  edge [
    source 62
    target 94
  ]
  edge [
    source 63
    target 61
  ]
  edge [
    source 63
    target 27
  ]
  edge [
    source 63
    target 73
  ]
  edge [
    source 63
    target 30
  ]
  edge [
    source 64
    target 161
  ]
  edge [
    source 64
    target 128
  ]
  edge [
    source 65
    target 61
  ]
  edge [
    source 65
    target 46
  ]
  edge [
    source 65
    target 144
  ]
  edge [
    source 65
    target 73
  ]
  edge [
    source 65
    target 30
  ]
  edge [
    source 66
    target 74
  ]
  edge [
    source 66
    target 37
  ]
  edge [
    source 66
    target 70
  ]
  edge [
    source 67
    target 142
  ]
  edge [
    source 68
    target 140
  ]
  edge [
    source 68
    target 105
  ]
  edge [
    source 68
    target 84
  ]
  edge [
    source 68
    target 157
  ]
  edge [
    source 68
    target 134
  ]
  edge [
    source 68
    target 50
  ]
  edge [
    source 68
    target 94
  ]
  edge [
    source 68
    target 119
  ]
  edge [
    source 70
    target 74
  ]
  edge [
    source 71
    target 31
  ]
  edge [
    source 73
    target 46
  ]
  edge [
    source 73
    target 144
  ]
  edge [
    source 73
    target 30
  ]
  edge [
    source 75
    target 43
  ]
  edge [
    source 76
    target 88
  ]
  edge [
    source 77
    target 45
  ]
  edge [
    source 77
    target 86
  ]
  edge [
    source 77
    target 23
  ]
  edge [
    source 77
    target 24
  ]
  edge [
    source 77
    target 55
  ]
  edge [
    source 78
    target 35
  ]
  edge [
    source 79
    target 37
  ]
  edge [
    source 79
    target 51
  ]
  edge [
    source 80
    target 69
  ]
  edge [
    source 80
    target 135
  ]
  edge [
    source 80
    target 44
  ]
  edge [
    source 80
    target 82
  ]
  edge [
    source 81
    target 146
  ]
  edge [
    source 81
    target 132
  ]
  edge [
    source 81
    target 133
  ]
  edge [
    source 82
    target 135
  ]
  edge [
    source 82
    target 69
  ]
  edge [
    source 83
    target 101
  ]
  edge [
    source 84
    target 119
  ]
  edge [
    source 85
    target 78
  ]
  edge [
    source 86
    target 23
  ]
  edge [
    source 86
    target 24
  ]
  edge [
    source 86
    target 3
  ]
  edge [
    source 86
    target 28
  ]
  edge [
    source 86
    target 129
  ]
  edge [
    source 86
    target 21
  ]
  edge [
    source 87
    target 149
  ]
  edge [
    source 87
    target 103
  ]
  edge [
    source 87
    target 59
  ]
  edge [
    source 87
    target 157
  ]
  edge [
    source 88
    target 29
  ]
  edge [
    source 89
    target 76
  ]
  edge [
    source 89
    target 102
  ]
  edge [
    source 89
    target 47
  ]
  edge [
    source 90
    target 169
  ]
  edge [
    source 90
    target 170
  ]
  edge [
    source 91
    target 26
  ]
  edge [
    source 91
    target 35
  ]
  edge [
    source 91
    target 25
  ]
  edge [
    source 92
    target 105
  ]
  edge [
    source 92
    target 167
  ]
  edge [
    source 92
    target 84
  ]
  edge [
    source 92
    target 50
  ]
  edge [
    source 92
    target 123
  ]
  edge [
    source 92
    target 157
  ]
  edge [
    source 92
    target 68
  ]
  edge [
    source 93
    target 138
  ]
  edge [
    source 93
    target 140
  ]
  edge [
    source 93
    target 52
  ]
  edge [
    source 93
    target 48
  ]
  edge [
    source 93
    target 123
  ]
  edge [
    source 93
    target 54
  ]
  edge [
    source 93
    target 7
  ]
  edge [
    source 95
    target 34
  ]
  edge [
    source 95
    target 35
  ]
  edge [
    source 95
    target 61
  ]
  edge [
    source 95
    target 65
  ]
  edge [
    source 95
    target 78
  ]
  edge [
    source 95
    target 26
  ]
  edge [
    source 96
    target 130
  ]
  edge [
    source 96
    target 58
  ]
  edge [
    source 96
    target 118
  ]
  edge [
    source 98
    target 34
  ]
  edge [
    source 98
    target 90
  ]
  edge [
    source 98
    target 61
  ]
  edge [
    source 98
    target 169
  ]
  edge [
    source 98
    target 156
  ]
  edge [
    source 99
    target 174
  ]
  edge [
    source 100
    target 149
  ]
  edge [
    source 102
    target 76
  ]
  edge [
    source 102
    target 88
  ]
  edge [
    source 102
    target 47
  ]
  edge [
    source 104
    target 32
  ]
  edge [
    source 104
    target 18
  ]
  edge [
    source 104
    target 116
  ]
  edge [
    source 104
    target 64
  ]
  edge [
    source 104
    target 161
  ]
  edge [
    source 105
    target 94
  ]
  edge [
    source 105
    target 157
  ]
  edge [
    source 105
    target 134
  ]
  edge [
    source 107
    target 115
  ]
  edge [
    source 107
    target 13
  ]
  edge [
    source 107
    target 4
  ]
  edge [
    source 107
    target 79
  ]
  edge [
    source 107
    target 136
  ]
  edge [
    source 107
    target 51
  ]
  edge [
    source 108
    target 98
  ]
  edge [
    source 108
    target 156
  ]
  edge [
    source 109
    target 103
  ]
  edge [
    source 110
    target 11
  ]
  edge [
    source 110
    target 42
  ]
  edge [
    source 110
    target 15
  ]
  edge [
    source 110
    target 2
  ]
  edge [
    source 110
    target 66
  ]
  edge [
    source 110
    target 70
  ]
  edge [
    source 110
    target 99
  ]
  edge [
    source 111
    target 11
  ]
  edge [
    source 111
    target 110
  ]
  edge [
    source 111
    target 15
  ]
  edge [
    source 111
    target 8
  ]
  edge [
    source 111
    target 2
  ]
  edge [
    source 111
    target 99
  ]
  edge [
    source 112
    target 110
  ]
  edge [
    source 112
    target 111
  ]
  edge [
    source 112
    target 99
  ]
  edge [
    source 113
    target 72
  ]
  edge [
    source 114
    target 172
  ]
  edge [
    source 114
    target 173
  ]
  edge [
    source 114
    target 25
  ]
  edge [
    source 115
    target 32
  ]
  edge [
    source 115
    target 120
  ]
  edge [
    source 115
    target 104
  ]
  edge [
    source 115
    target 136
  ]
  edge [
    source 116
    target 161
  ]
  edge [
    source 116
    target 151
  ]
  edge [
    source 116
    target 64
  ]
  edge [
    source 117
    target 88
  ]
  edge [
    source 117
    target 71
  ]
  edge [
    source 117
    target 76
  ]
  edge [
    source 118
    target 58
  ]
  edge [
    source 119
    target 49
  ]
  edge [
    source 119
    target 134
  ]
  edge [
    source 120
    target 100
  ]
  edge [
    source 120
    target 32
  ]
  edge [
    source 120
    target 151
  ]
  edge [
    source 121
    target 106
  ]
  edge [
    source 121
    target 67
  ]
  edge [
    source 122
    target 143
  ]
  edge [
    source 122
    target 106
  ]
  edge [
    source 123
    target 140
  ]
  edge [
    source 123
    target 149
  ]
  edge [
    source 123
    target 103
  ]
  edge [
    source 123
    target 177
  ]
  edge [
    source 123
    target 87
  ]
  edge [
    source 123
    target 59
  ]
  edge [
    source 123
    target 157
  ]
  edge [
    source 124
    target 31
  ]
  edge [
    source 125
    target 150
  ]
  edge [
    source 125
    target 0
  ]
  edge [
    source 127
    target 46
  ]
  edge [
    source 127
    target 65
  ]
  edge [
    source 127
    target 144
  ]
  edge [
    source 127
    target 27
  ]
  edge [
    source 129
    target 28
  ]
  edge [
    source 129
    target 3
  ]
  edge [
    source 129
    target 21
  ]
  edge [
    source 131
    target 138
  ]
  edge [
    source 131
    target 52
  ]
  edge [
    source 131
    target 6
  ]
  edge [
    source 131
    target 7
  ]
  edge [
    source 132
    target 146
  ]
  edge [
    source 132
    target 37
  ]
  edge [
    source 132
    target 66
  ]
  edge [
    source 132
    target 128
  ]
  edge [
    source 132
    target 133
  ]
  edge [
    source 133
    target 146
  ]
  edge [
    source 133
    target 74
  ]
  edge [
    source 133
    target 37
  ]
  edge [
    source 133
    target 66
  ]
  edge [
    source 134
    target 94
  ]
  edge [
    source 136
    target 79
  ]
  edge [
    source 136
    target 104
  ]
  edge [
    source 136
    target 51
  ]
  edge [
    source 137
    target 56
  ]
  edge [
    source 137
    target 169
  ]
  edge [
    source 137
    target 34
  ]
  edge [
    source 138
    target 139
  ]
  edge [
    source 138
    target 89
  ]
  edge [
    source 138
    target 48
  ]
  edge [
    source 138
    target 47
  ]
  edge [
    source 139
    target 89
  ]
  edge [
    source 139
    target 102
  ]
  edge [
    source 139
    target 47
  ]
  edge [
    source 140
    target 157
  ]
  edge [
    source 140
    target 48
  ]
  edge [
    source 140
    target 54
  ]
  edge [
    source 141
    target 29
  ]
  edge [
    source 141
    target 142
  ]
  edge [
    source 141
    target 53
  ]
  edge [
    source 141
    target 14
  ]
  edge [
    source 142
    target 53
  ]
  edge [
    source 142
    target 29
  ]
  edge [
    source 144
    target 61
  ]
  edge [
    source 144
    target 46
  ]
  edge [
    source 144
    target 30
  ]
  edge [
    source 145
    target 1
  ]
  edge [
    source 145
    target 96
  ]
  edge [
    source 145
    target 70
  ]
  edge [
    source 145
    target 118
  ]
  edge [
    source 145
    target 58
  ]
  edge [
    source 147
    target 31
  ]
  edge [
    source 147
    target 124
  ]
  edge [
    source 147
    target 148
  ]
  edge [
    source 148
    target 71
  ]
  edge [
    source 148
    target 31
  ]
  edge [
    source 148
    target 124
  ]
  edge [
    source 149
    target 151
  ]
  edge [
    source 149
    target 103
  ]
  edge [
    source 149
    target 109
  ]
  edge [
    source 150
    target 0
  ]
  edge [
    source 152
    target 172
  ]
  edge [
    source 152
    target 173
  ]
  edge [
    source 152
    target 114
  ]
  edge [
    source 153
    target 116
  ]
  edge [
    source 153
    target 151
  ]
  edge [
    source 153
    target 64
  ]
  edge [
    source 154
    target 9
  ]
  edge [
    source 154
    target 13
  ]
  edge [
    source 154
    target 107
  ]
  edge [
    source 154
    target 4
  ]
  edge [
    source 154
    target 79
  ]
  edge [
    source 154
    target 136
  ]
  edge [
    source 154
    target 51
  ]
  edge [
    source 155
    target 12
  ]
  edge [
    source 155
    target 122
  ]
  edge [
    source 157
    target 94
  ]
  edge [
    source 158
    target 154
  ]
  edge [
    source 158
    target 8
  ]
  edge [
    source 158
    target 111
  ]
  edge [
    source 158
    target 37
  ]
  edge [
    source 158
    target 79
  ]
  edge [
    source 158
    target 9
  ]
  edge [
    source 158
    target 66
  ]
  edge [
    source 158
    target 112
  ]
  edge [
    source 158
    target 51
  ]
  edge [
    source 158
    target 132
  ]
  edge [
    source 158
    target 165
  ]
  edge [
    source 159
    target 34
  ]
  edge [
    source 159
    target 61
  ]
  edge [
    source 159
    target 163
  ]
  edge [
    source 159
    target 33
  ]
  edge [
    source 159
    target 98
  ]
  edge [
    source 159
    target 108
  ]
  edge [
    source 159
    target 30
  ]
  edge [
    source 160
    target 62
  ]
  edge [
    source 160
    target 113
  ]
  edge [
    source 160
    target 72
  ]
  edge [
    source 162
    target 16
  ]
  edge [
    source 163
    target 34
  ]
  edge [
    source 163
    target 35
  ]
  edge [
    source 163
    target 61
  ]
  edge [
    source 163
    target 78
  ]
  edge [
    source 163
    target 95
  ]
  edge [
    source 163
    target 26
  ]
  edge [
    source 164
    target 101
  ]
  edge [
    source 164
    target 6
  ]
  edge [
    source 165
    target 37
  ]
  edge [
    source 165
    target 107
  ]
  edge [
    source 165
    target 57
  ]
  edge [
    source 165
    target 66
  ]
  edge [
    source 165
    target 79
  ]
  edge [
    source 165
    target 51
  ]
  edge [
    source 166
    target 172
  ]
  edge [
    source 166
    target 35
  ]
  edge [
    source 166
    target 26
  ]
  edge [
    source 166
    target 25
  ]
  edge [
    source 166
    target 91
  ]
  edge [
    source 166
    target 114
  ]
  edge [
    source 167
    target 142
  ]
  edge [
    source 167
    target 141
  ]
  edge [
    source 167
    target 84
  ]
  edge [
    source 167
    target 139
  ]
  edge [
    source 167
    target 67
  ]
  edge [
    source 168
    target 149
  ]
  edge [
    source 168
    target 151
  ]
  edge [
    source 168
    target 115
  ]
  edge [
    source 168
    target 116
  ]
  edge [
    source 168
    target 153
  ]
  edge [
    source 168
    target 100
  ]
  edge [
    source 168
    target 120
  ]
  edge [
    source 168
    target 136
  ]
  edge [
    source 170
    target 3
  ]
  edge [
    source 170
    target 169
  ]
  edge [
    source 171
    target 138
  ]
  edge [
    source 171
    target 52
  ]
  edge [
    source 171
    target 131
  ]
  edge [
    source 172
    target 173
  ]
  edge [
    source 174
    target 173
  ]
  edge [
    source 174
    target 60
  ]
  edge [
    source 174
    target 56
  ]
  edge [
    source 174
    target 137
  ]
  edge [
    source 175
    target 90
  ]
  edge [
    source 175
    target 169
  ]
  edge [
    source 175
    target 23
  ]
  edge [
    source 175
    target 24
  ]
  edge [
    source 175
    target 56
  ]
  edge [
    source 175
    target 28
  ]
  edge [
    source 175
    target 170
  ]
  edge [
    source 177
    target 103
  ]
]
