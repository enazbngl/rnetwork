graph [
  directed 1
  node [
    id 0
    label "ARG44"
  ]
  node [
    id 1
    label "ASP30"
  ]
  node [
    id 2
    label "LYS176"
  ]
  node [
    id 3
    label "GLY112"
  ]
  node [
    id 4
    label "GLY175"
  ]
  node [
    id 5
    label "VAL165"
  ]
  node [
    id 6
    label "TYR171"
  ]
  node [
    id 7
    label "TYR113"
  ]
  node [
    id 8
    label "TRP60"
  ]
  node [
    id 9
    label "LEU160"
  ]
  node [
    id 10
    label "ARG157"
  ]
  node [
    id 11
    label "LEU130"
  ]
  node [
    id 12
    label "PHE33"
  ]
  node [
    id 13
    label "VAL103"
  ]
  node [
    id 14
    label "TYR59"
  ]
  node [
    id 15
    label "GLU55"
  ]
  node [
    id 16
    label "ALA158"
  ]
  node [
    id 17
    label "SER4"
  ]
  node [
    id 18
    label "SER2"
  ]
  node [
    id 19
    label "VAL25"
  ]
  node [
    id 20
    label "CYS164"
  ]
  node [
    id 21
    label "VAL28"
  ]
  node [
    id 22
    label "TYR378"
  ]
  node [
    id 23
    label "GLU161"
  ]
  node [
    id 24
    label "LEU168"
  ]
  node [
    id 25
    label "GLU376"
  ]
  node [
    id 26
    label "GLU166"
  ]
  node [
    id 27
    label "HIS3"
  ]
  node [
    id 28
    label "LEU163"
  ]
  node [
    id 29
    label "GLY100"
  ]
  node [
    id 30
    label "ARG108"
  ]
  node [
    id 31
    label "GLY162"
  ]
  node [
    id 32
    label "CYS101"
  ]
  node [
    id 33
    label "LEU109"
  ]
  node [
    id 34
    label "GLU128"
  ]
  node [
    id 35
    label "ARG6"
  ]
  node [
    id 36
    label "GLU46"
  ]
  node [
    id 37
    label "ASP102"
  ]
  node [
    id 38
    label "GLU377"
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 2
    target 6
  ]
  edge [
    source 3
    target 10
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 13
  ]
  edge [
    source 3
    target 11
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 5
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 9
  ]
  edge [
    source 3
    target 7
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 4
    target 6
  ]
  edge [
    source 4
    target 2
  ]
  edge [
    source 6
    target 5
  ]
  edge [
    source 7
    target 10
  ]
  edge [
    source 7
    target 11
  ]
  edge [
    source 7
    target 34
  ]
  edge [
    source 7
    target 29
  ]
  edge [
    source 7
    target 9
  ]
  edge [
    source 7
    target 32
  ]
  edge [
    source 8
    target 36
  ]
  edge [
    source 8
    target 14
  ]
  edge [
    source 8
    target 15
  ]
  edge [
    source 8
    target 0
  ]
  edge [
    source 9
    target 16
  ]
  edge [
    source 9
    target 33
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 20
  ]
  edge [
    source 9
    target 22
  ]
  edge [
    source 9
    target 11
  ]
  edge [
    source 9
    target 23
  ]
  edge [
    source 9
    target 26
  ]
  edge [
    source 9
    target 5
  ]
  edge [
    source 9
    target 28
  ]
  edge [
    source 9
    target 29
  ]
  edge [
    source 9
    target 31
  ]
  edge [
    source 9
    target 32
  ]
  edge [
    source 10
    target 20
  ]
  edge [
    source 10
    target 16
  ]
  edge [
    source 10
    target 31
  ]
  edge [
    source 11
    target 16
  ]
  edge [
    source 11
    target 23
  ]
  edge [
    source 11
    target 10
  ]
  edge [
    source 12
    target 6
  ]
  edge [
    source 13
    target 17
  ]
  edge [
    source 13
    target 33
  ]
  edge [
    source 13
    target 20
  ]
  edge [
    source 13
    target 5
  ]
  edge [
    source 13
    target 24
  ]
  edge [
    source 13
    target 27
  ]
  edge [
    source 13
    target 30
  ]
  edge [
    source 13
    target 32
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 6
  ]
  edge [
    source 16
    target 20
  ]
  edge [
    source 16
    target 31
  ]
  edge [
    source 17
    target 20
  ]
  edge [
    source 17
    target 5
  ]
  edge [
    source 18
    target 30
  ]
  edge [
    source 18
    target 13
  ]
  edge [
    source 18
    target 17
  ]
  edge [
    source 18
    target 27
  ]
  edge [
    source 19
    target 36
  ]
  edge [
    source 19
    target 12
  ]
  edge [
    source 20
    target 31
  ]
  edge [
    source 20
    target 5
  ]
  edge [
    source 21
    target 17
  ]
  edge [
    source 21
    target 12
  ]
  edge [
    source 21
    target 1
  ]
  edge [
    source 21
    target 24
  ]
  edge [
    source 21
    target 4
  ]
  edge [
    source 21
    target 27
  ]
  edge [
    source 21
    target 6
  ]
  edge [
    source 21
    target 29
  ]
  edge [
    source 21
    target 37
  ]
  edge [
    source 21
    target 32
  ]
  edge [
    source 22
    target 20
  ]
  edge [
    source 22
    target 29
  ]
  edge [
    source 22
    target 16
  ]
  edge [
    source 22
    target 28
  ]
  edge [
    source 23
    target 16
  ]
  edge [
    source 23
    target 33
  ]
  edge [
    source 23
    target 10
  ]
  edge [
    source 23
    target 20
  ]
  edge [
    source 23
    target 26
  ]
  edge [
    source 23
    target 5
  ]
  edge [
    source 23
    target 28
  ]
  edge [
    source 23
    target 30
  ]
  edge [
    source 23
    target 31
  ]
  edge [
    source 24
    target 17
  ]
  edge [
    source 24
    target 20
  ]
  edge [
    source 24
    target 28
  ]
  edge [
    source 24
    target 5
  ]
  edge [
    source 24
    target 6
  ]
  edge [
    source 24
    target 29
  ]
  edge [
    source 25
    target 20
  ]
  edge [
    source 25
    target 22
  ]
  edge [
    source 25
    target 28
  ]
  edge [
    source 25
    target 24
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 5
  ]
  edge [
    source 25
    target 6
  ]
  edge [
    source 25
    target 29
  ]
  edge [
    source 25
    target 14
  ]
  edge [
    source 25
    target 8
  ]
  edge [
    source 25
    target 38
  ]
  edge [
    source 26
    target 20
  ]
  edge [
    source 26
    target 28
  ]
  edge [
    source 26
    target 24
  ]
  edge [
    source 26
    target 5
  ]
  edge [
    source 26
    target 6
  ]
  edge [
    source 26
    target 30
  ]
  edge [
    source 26
    target 31
  ]
  edge [
    source 27
    target 17
  ]
  edge [
    source 27
    target 24
  ]
  edge [
    source 28
    target 20
  ]
  edge [
    source 28
    target 16
  ]
  edge [
    source 28
    target 31
  ]
  edge [
    source 28
    target 5
  ]
  edge [
    source 29
    target 20
  ]
  edge [
    source 29
    target 17
  ]
  edge [
    source 29
    target 5
  ]
  edge [
    source 30
    target 5
  ]
  edge [
    source 31
    target 5
  ]
  edge [
    source 32
    target 17
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 20
  ]
  edge [
    source 32
    target 27
  ]
  edge [
    source 32
    target 23
  ]
  edge [
    source 32
    target 24
  ]
  edge [
    source 32
    target 5
  ]
  edge [
    source 32
    target 29
  ]
  edge [
    source 32
    target 30
  ]
  edge [
    source 33
    target 17
  ]
  edge [
    source 33
    target 20
  ]
  edge [
    source 33
    target 5
  ]
  edge [
    source 33
    target 26
  ]
  edge [
    source 33
    target 27
  ]
  edge [
    source 33
    target 30
  ]
  edge [
    source 33
    target 31
  ]
  edge [
    source 34
    target 11
  ]
  edge [
    source 34
    target 10
  ]
  edge [
    source 35
    target 17
  ]
  edge [
    source 35
    target 19
  ]
  edge [
    source 35
    target 20
  ]
  edge [
    source 35
    target 21
  ]
  edge [
    source 35
    target 1
  ]
  edge [
    source 35
    target 24
  ]
  edge [
    source 35
    target 25
  ]
  edge [
    source 35
    target 38
  ]
  edge [
    source 35
    target 3
  ]
  edge [
    source 35
    target 29
  ]
  edge [
    source 35
    target 7
  ]
  edge [
    source 35
    target 37
  ]
  edge [
    source 35
    target 32
  ]
  edge [
    source 36
    target 12
  ]
  edge [
    source 37
    target 17
  ]
  edge [
    source 37
    target 9
  ]
  edge [
    source 37
    target 33
  ]
  edge [
    source 37
    target 20
  ]
  edge [
    source 37
    target 13
  ]
  edge [
    source 37
    target 24
  ]
  edge [
    source 37
    target 18
  ]
  edge [
    source 37
    target 5
  ]
  edge [
    source 37
    target 29
  ]
  edge [
    source 37
    target 30
  ]
  edge [
    source 37
    target 7
  ]
  edge [
    source 37
    target 27
  ]
  edge [
    source 37
    target 32
  ]
  edge [
    source 38
    target 20
  ]
  edge [
    source 38
    target 29
  ]
  edge [
    source 38
    target 9
  ]
  edge [
    source 38
    target 28
  ]
  edge [
    source 38
    target 22
  ]
]
